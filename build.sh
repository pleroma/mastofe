#!/bin/sh
TARGET="${TARGET:-../pleroma}" # Where pleroma’s repository is sitting
static_dir="priv/static" # Set this to instance/static when testing production build

die() {
	echo "Die: $@"
	exit 1
}

[ -d "${TARGET}/priv/static" ] || die "${TARGET}/priv/static directory is missing, are you sure TARGET is set to a pleroma repository? (Info: TARGET=${TARGET} )"

yarn install -D || die "Installing dependencies via yarn failed"

rm -rf public/packs public/assets
env -i "PATH=$PATH" npm run build || die "Building the frontend failed"
cp public/assets/sw.js "${TARGET}/${static_dir}/sw.js" || die "installing sw.js (service-worker) failed"
rm -rf "${TARGET}/${static_dir}/packs" || die "Removing old assets in priv/static/packs failed"
cp -r public/packs "${TARGET}/${static_dir}/packs" || die "Copying new assets in priv/static/packs failed"
rm -rf "${TARGET}/${static_dir}/emoji/*.svg" || die "Removing the old emoji assets failed"
cp -r public/emoji/* "${TARGET}/${static_dir}/emoji" || die "Installing the new emoji assets failed"
