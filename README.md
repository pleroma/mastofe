# Mastodon Frontend, Glitch-soc + Pleroma Edition

Here is a distribution of the glitch-soc frontend for pleroma. Everything from the upstream repository is kept and rebased on for easy updates, this does screws up on Merge Requests so they’ll be treated as a patchset if done here.

# Fork vs. Distribution
This repository is not a fork of glitch-soc, there is only a few patches on it that are kept and designed to be easily removable if it needs to (for example when upstream does one of our features on their side too).
This also means that there is no merge, only one big rebase to get the patches up-to-date. See the Development section for more information.

# Deployement
## Fetching CI artifacts
Pleroma (backend) has a shell script in `installation/download-mastofe-build.sh` which can be used to get a build from the CI artifacts, by default it will install the one from `rebase/glitch-soc` into `instance/static` (so you can revert to the bundled frontend by removing it and it is free from git conflicts), this behaviour can be changed by modifying the first lines of the script.

## Updating the bundle
This is what you want to do to update the mastofe bundled with pleroma.

- Run ``build.sh`` at the root of this repo, you can set the ``TARGET`` environment variable if pleroma isn’t at ``../pleroma`` (default value of ``TARGET``)
- Go to pleroma repo:
  - ``git add priv/static/sw.js priv/static/packs``
  - ``git commit -m "update mastofe"``

# Development
## Branches
- `pleroma` branch which get re-referenced to `rebase/glitch-soc` once it is stable
- `master`: Same branch as upstream repository, should be updated before doing an updating rebase
- `rebase/glitch-soc`: branch which rebases from upstream, used for testing

For developement/Merge Requests I would suggest to use `master` when you are introducing new modifications that cannot be in the upstream, and when you are changing current modifications prefer `rebase/glitch-soc`.

Never use `pleroma` as a base for Merge Requests, it is not meant to be modified directly.

## Changelog
There is a `Changelog-mastofe.txt` file used to do the release notes for the message on tagging a new release. It is cleaned when updating from upstream (`git filter-branch` should be a nice friend).
Changes on the behaviour of the frontend should be noted in this file together with their modification (commit at best, MR at worst).

## Tools
- Node.js
- yarn (preferred) or npm
- HTTP proxy (such as nginx)

## nginx setup
I'll assume that you have already fired up pleroma using the installation guide. To work on the frontend while still having the backend up, use this nginx config.

```
server {
    listen 80;
    server_name pleroma.testing;

    location /packs {
        add_header 'Access-Control-Allow-Origin' '*';
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;

        proxy_pass http://localhost:3035;
    }

    location / {
        add_header 'Access-Control-Allow-Origin' '*';
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $http_host;

        proxy_pass http://localhost:4000;
    }
}
```

Change the `server_name` if you like. I personally like to create a new entry in /etc/hosts and add `127.0.0.1 pleroma.testing`, but you do what suits you.

## Running
- Getting the node dependencies is done with `yarn install -D` (or `npm install` if you don’t have yarn)
- Launching the frontend is done with `npm run dev`. It should be reachable once it finnishes compiling.
