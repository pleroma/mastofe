import Rails from '@rails/ujs';
import { signOutLink } from 'flavours/glitch/util/backend_links';

export const logOut = () => {
  fetch(signOutLink, {
    method: 'DELETE'
  }).then(() => {
    window.location = '/web/';
  });
};
