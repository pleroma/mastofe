export const preferencesLink = undefined; // '/settings/preferences';
export const profileLink = undefined; // '/settings/profile';
export const signOutLink = '/auth/sign_out';
export const termsLink = undefined; // '/terms';
export const accountAdminLink = (id) => `/pleroma/admin/#/users/${id}/`;
export const statusAdminLink = (account_id, status_id) => `/pleroma/admin/#/users/${account_id}/`;
export const filterEditLink = undefined; // (id) => `/filters/${id}/edit`;
export const relationshipsLink = undefined; // = '/relationships';
export const securityLink = undefined ; // = '/auth/edit';
