import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

export default @injectIntl
class LinkFooter extends React.PureComponent {

  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  render () {
    return (
      <div className='getting-started__footer'>
        <p>
          <FormattedMessage
            id='getting_started.mastofe_notice'
            defaultMessage='Mastofe is a libre distribution of the frontend from {glitchsoc}, a friendly fork of {mastodon}. You can contribute or report issues at {mastofe}.'
            values={{
              mastofe: <a href='https://git.pleroma.social/pleroma/mastofe' rel='noopener noreferrer' target='_blank'>Mastofe</a>,
              glitchsoc: <a href='https://github.com/glitch-soc/mastodon' rel='noopener noreferrer' target='_blank'>glitch-soc</a>,
              mastodon: <a href='https://github.com/tootsuite/mastodon' rel='noopener noreferrer' target='_blank'>Mastodon</a>,
              pleroma: <a href='https://pleroma.social' rel='noopener noreferrer' target='_blank'>Pleroma</a>
            }}
          />
        </p>
      </div>
    );
  }
};
