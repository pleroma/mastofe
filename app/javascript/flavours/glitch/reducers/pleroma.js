import { Map as ImmutableMap } from 'immutable';
import { PANEL_FETCH_SUCCESS, PLEROMA_CONFIG_FETCH_SUCCESS } from 'mastodon/actions/pleroma';

const initialPanel = ImmutableMap({
  enabled: false,
  panel: ''
});

export default function custom_panel(state = initialPanel, action) {
  switch (action.type) {
  case PANEL_FETCH_SUCCESS:
    return state.set('panel', action.panel); break;
  case PLEROMA_CONFIG_FETCH_SUCCESS:
    return state.set('enabled', (action.config || {}).showInstanceSpecificPanel);
  }

  return state;
};
